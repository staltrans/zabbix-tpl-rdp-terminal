@echo off

set login=
set pwdhash=

:: https://www.donkz.nl
if defined ProgramFiles(x86) (
  set rdplus="%ProgramFiles(x86)%\Remote Desktop Plus\rdp.exe"
) else (
  set rdplus="%ProgramFiles%\Remote Desktop Plus\rdp.exe"
)

if exist %rdplus% (
	%rdplus% /v:localhost /u:%login% /pe:%pwdhash% /batch /eventlog /wait
	echo %ERRORLEVEL%
) else (
	echo 99999
)
::
:: 0: No errors.
:: 1: General undefined error.
:: 2: Specified connection file not found.
:: 5: Target computer not in the list of allowed targets.
:: 29: Cannot write to the log file.
:: 30: Profile not found.
:: 87: Invalid command line parameter or combination of command line parameters.
:: 161: The format of the specified path is invalid (filename of the connection file, log file or command options file).
:: 259: No responsive server found in the list (when using /lb).
:: 2382: Invalid notation of the specified host name or IP address.
:: 10060: Connection timed out to the specified server.
:: 11001: Remote hostname not found.
::
:: 99999: Remote Desktop Plus not found
::